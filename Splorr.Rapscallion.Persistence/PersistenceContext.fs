﻿namespace Splorr.Rapscallion.Persistence

open Splorr.Common
open Splorr.Rapscallion.Business
open System.Data.SQLite

type PersistenceContext()=
    let connection = new SQLiteConnection("Data Source=:memory:;Version=3;New=True;")
    do
        connection.Open()
    interface CommonContext
    interface BusinessContext
    interface SessionIdentifierRepository.CreateContext with
        member this.sessionIdentifierSource = ref SessionIdentifierImplementation.Create
    interface SessionLocation.GetContext with
        member this.sessionLocationSource = ref (SessionLocationImplementation.Get connection)
    interface SessionLocation.PutContext with
        member this.sessionLocationSink = ref (SessionLocationImplementation.Put connection)


