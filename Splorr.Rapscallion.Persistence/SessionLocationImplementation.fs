﻿namespace Splorr.Rapscallion.Persistence

open Splorr.Rapscallion.Model
open System.Data.SQLite
open System

module internal SessionLocationImplementation =
    let private CreateDataStore (connection : SQLiteConnection) : unit =
        use command = new SQLiteCommand("CREATE TABLE IF NOT EXISTS [SessionLocations] ([SessionId] TEXT, [LocationX] INTEGER NOT NULL, [LocationY] INTEGER NOT NULL, PRIMARY KEY([SessionId]));",connection)
        command.ExecuteNonQuery()
        |> ignore

    let internal Get (connection:SQLiteConnection) (session : SessionIdentifier) : Location option =
        CreateDataStore connection
        use command = new SQLiteCommand("SELECT [LocationX], [LocationY] FROM [SessionLocations] WHERE [SessionId]= $sessionId;", connection)
        command.Parameters.AddWithValue("$sessionId", session.ToString()) |> ignore
        let reader = command.ExecuteReader()
        if reader.Read() then
            (reader.GetInt32(0), reader.GetInt32(1)) |> Some
        else
            None

    let internal Put (connection:SQLiteConnection) (session : SessionIdentifier, location : Location option) : unit =
        CreateDataStore connection
        match location with
        | Some l ->
            use command = new SQLiteCommand("REPLACE INTO [SessionLocations] ([SessionId],[LocationX],[LocationY]) VALUES ($sessionId,$locationX,$locationY);", connection)
            command.Parameters.AddWithValue("$sessionId", session.ToString()) |> ignore
            command.Parameters.AddWithValue("$locationX", l |> fst) |> ignore
            command.Parameters.AddWithValue("$locationY", l |> snd) |> ignore
            command.ExecuteNonQuery() |> ignore
        | None ->
            use command = new SQLiteCommand("DELETE FROM [SessionLocations] WHERE [SessionId] = $sessionId;", connection)
            command.Parameters.AddWithValue("$sessionId", session.ToString()) |> ignore
            command.ExecuteNonQuery() |> ignore

