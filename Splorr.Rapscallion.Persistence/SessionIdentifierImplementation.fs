﻿namespace Splorr.Rapscallion.Persistence

open Splorr.Rapscallion.Model
open System

module internal SessionIdentifierImplementation =
    let internal Create () : SessionIdentifier = Guid.NewGuid()