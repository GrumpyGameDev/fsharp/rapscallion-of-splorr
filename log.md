Day #5: Test Driven Development in F# of a Roguelike Game
# Day 5 - 20201117

* sqlite as data store

# Day 4 - 20201116

* refactoring host game
* pushing down position for session
* session identifier
* changed hues
* switching field representation to use location type

# Day 3 - 20201115

* made a start of a tileset
* started laying out the screen

# Day 2 - 20201114

* scaffolding
* dude moving on a screen

# Day 1 - 20201113

* The beginning
* deciding what "is" a roguelike