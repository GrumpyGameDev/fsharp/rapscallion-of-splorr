﻿namespace Splorr.Rapscallion

open Microsoft.Xna.Framework
open Microsoft.Xna.Framework.Graphics
open System
open Splorr.Rapscallion.Presentation

module internal HostGameUtility =
    let internal sliceDestinations
            (columns: int, rows: int) 
            (offsetX: int, offsetY: int)
            (cellWidth: int, cellHeight: int)
            : Map<int * int, Rectangle> =
        [for column=0 to (columns - 1) do
            for row=0 to (rows - 1) do yield (column, row)]
        |> List.map
            (fun (column, row) ->
                ((column, row), 
                    Rectangle
                        (offsetX + column * cellWidth,
                        offsetY + row * cellHeight, 
                        cellWidth, 
                        cellHeight)))
        |> Map.ofList

    let internal sliceSources 
            (columns : int, rows : int)
            (texture: Texture2D) 
            : Map<int, Nullable<Rectangle>> =
        let tileWidth = texture.Width / columns
        let tileHeight = texture.Height / rows
        [0..(columns * rows - 1)]
        |> List.map
            (fun index ->
                let column = index % columns
                let row = index / columns
                (index, Nullable<Rectangle>(Rectangle(column * tileWidth, row * tileHeight, tileWidth, tileHeight))))
        |> Map.ofList

    let internal HueToColor =
        function 
        | Black      -> Color(  0,   0,   0)
        | DarkGray   -> Color( 87,  87,  87)
        | Red        -> Color(173,  35,  35)
        | Blue       -> Color( 42,  75, 215)
        | Green      -> Color( 29, 105,  20)
        | Brown      -> Color(129,  74,  25)
        | Purple     -> Color(129,  38, 192)
        | LightGray  -> Color(160, 160, 160)
        | LightGreen -> Color(129, 197, 122)
        | LightBlue  -> Color(157, 175, 255)
        | Cyan       -> Color( 41, 208, 208)
        | Orange     -> Color(255, 146,  51)
        | Yellow     -> Color(255, 238,  51)
        | Tan        -> Color(233, 222, 187)
        | Pink       -> Color(255, 205, 243)
        | White      -> Color(255, 255, 255)






