﻿namespace Splorr.Rapscallion

open System
open Splorr.Rapscallion.Presentation

module internal MessagesImplementation =
    let private messagesSize : (int * int) ref = ref (1,1)
    let private messages : (Hue * string) list ref = ref []

    let internal SetSize (size : int * int) : unit =
        messagesSize := size

    let rec internal Put (hue: Hue, text:string) : unit =
        let columns, rows = messagesSize.Value
        if text.Length > columns then
            let first = text.Substring(0, columns)
            let second = text.Substring(columns)
            Put (hue, first)
            Put (hue, second)
        else
            messages := List.append messages.Value [ (hue, text) ]
        let actualRows = messages.Value.Length
        if actualRows > rows then
            messages := 
                messages.Value
                |> List.skip (actualRows - rows)

    let internal Get () : (Hue * string) list =
        messages.Value
    