﻿namespace Splorr.Rapscallion

open System
open Splorr.Rapscallion.Presentation
open Splorr.Rapscallion.Model

module internal FieldImplementation =
    let private fieldSize : Location ref = ref (0,0)
    let private fieldCells : Map<Location, DisplayCell> ref = ref Map.empty 

    let internal SetSize (size : int * int) : unit =
        fieldSize := size

    let internal PutCell (location : Location, cell: DisplayCell option) : unit =
        match cell with
        | None ->
            fieldCells := 
                fieldCells.Value
                |> Map.remove location
        | Some c ->
            fieldCells := 
                fieldCells.Value
                |> Map.add location c
    
    let internal GetCells () : (Location * DisplayCell) list =
        fieldCells.Value
        |> Map.toList

    