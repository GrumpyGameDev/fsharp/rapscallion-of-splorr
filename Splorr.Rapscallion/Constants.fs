﻿namespace Splorr.Rapscallion

module internal Constants =
    let internal CellWidth = 16
    let internal CellHeight = 16
    let internal CellColumns = 80
    let internal CellRows = 45
    let internal BackBufferWidth = CellWidth * CellColumns
    let internal BackBufferHeight = CellHeight * CellRows

    let internal FieldCellWidth = 16
    let internal FieldCellHeight = 16
    let internal FieldColumns = 45
    let internal FieldRows = 45
    let internal FieldOffsetX = 288
    let internal FieldOffsetY = 0
    
    let internal MessageCellWidth = 8
    let internal MessageCellHeight = 8
    let internal MessageColumns = 32
    let internal MessageRows = 56
    let internal MessageOffsetX = 1024
    let internal MessageOffsetY = 272

