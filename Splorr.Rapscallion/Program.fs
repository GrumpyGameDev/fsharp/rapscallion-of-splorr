﻿open System
open Splorr.Rapscallion

[<EntryPoint>]
let main argv =
    let context = RapscallionContext()
    use game = new HostGame(context)
    game.Run()
    0
