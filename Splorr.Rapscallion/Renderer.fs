﻿namespace Splorr.Rapscallion

open Microsoft.Xna.Framework.Graphics
open Splorr.Common
open Microsoft.Xna.Framework
open Splorr.Rapscallion.Presentation
open System.IO
open System
open Splorr.Rapscallion.Model

module internal Renderer =
    let private layoutTexture : Texture2D ref = ref null
    let private romfontTexture : Texture2D ref = ref null
    let private romfontSourceRectangles : Map<int, Nullable<Rectangle>> ref = ref Map.empty

    let private tilesTexture : Texture2D ref = ref null
    let private tilesSourceRectangles : Map<int, Nullable<Rectangle>> ref = ref Map.empty

    let private blank : Texture2D ref = ref null

    let private fieldDestinations : Map<int * int, Rectangle> =
        HostGameUtility.sliceDestinations (Constants.FieldColumns, Constants.FieldRows) (Constants.FieldOffsetX, Constants.FieldOffsetY) (Constants.FieldCellWidth, Constants.FieldCellHeight)
    let private messageDestinations : Map<int * int, Rectangle> =
        HostGameUtility.sliceDestinations (Constants.MessageColumns, Constants.MessageRows) (Constants.MessageOffsetX, Constants.MessageOffsetY) (Constants.MessageCellWidth, Constants.MessageCellHeight)

    let private LoadRomfont(graphicsDevice : GraphicsDevice) : unit =
          romfontTexture := Texture2D.FromStream(graphicsDevice, new FileStream("Content/romfont8x8.png", FileMode.Open))
          romfontSourceRectangles := HostGameUtility.sliceSources (16,16) romfontTexture.Value

    let private LoadTiles(graphicsDevice : GraphicsDevice) : unit =
          tilesTexture := Texture2D.FromStream(graphicsDevice, new FileStream("Content/tiles.png", FileMode.Open))
          tilesSourceRectangles := HostGameUtility.sliceSources (16, 16) tilesTexture.Value

    let internal Load(graphicsDevice : GraphicsDevice) : unit =
        layoutTexture := Texture2D.FromStream(graphicsDevice, new FileStream("Content/layout.png", FileMode.Open))
        blank := Texture2D.FromStream(graphicsDevice, new FileStream("Content/blank.png", FileMode.Open))
        LoadRomfont graphicsDevice
        LoadTiles graphicsDevice

    let private RenderLayout
            (spriteBatch : SpriteBatch) 
            : unit =
        spriteBatch.Draw(layoutTexture.Value, Vector2(0.0f, 0.0f), Color.White)

    let private RenderField
            (context : CommonContext)
            (session : SessionIdentifier)
            (spriteBatch : SpriteBatch) 
            : unit =
        // temporary vvv
        Business.Game.GetLocationForSession context session
        |> Option.iter
            (fun position ->
                Presentation.Display.PutFieldCell context (position, Some {backgroundHue=Black;foregroundHue=Tan;glyph=0uy}))
        // temporary ^^^

        Presentation.Display.GetFieldCells context
        |> List.iter
            (fun ((column, row), cell) ->
                fieldDestinations 
                |> Map.tryFind (column, row)
                |> Option.iter (fun destination ->
                    spriteBatch.Draw(blank.Value, destination, cell.backgroundHue |> HostGameUtility.HueToColor)
                    spriteBatch.Draw(tilesTexture.Value, destination, tilesSourceRectangles.Value.[cell.glyph |> int], cell.foregroundHue |> HostGameUtility.HueToColor)))

        // temporary vvv
        Business.Game.GetLocationForSession context session
        |> Option.iter
            (fun position ->
                Presentation.Display.PutFieldCell context (position, None))
        // temporary ^^^


    let private RenderCharacter
            (spriteBatch : SpriteBatch)
            (column : int ref, row : int)
            (hue : Hue, character : char)
            : unit =
        spriteBatch.Draw
            (romfontTexture.Value, 
            messageDestinations.[(column.Value, row)], 
            romfontSourceRectangles.Value.[character |> int], 
            hue |> HostGameUtility.HueToColor)
        column := column.Value + 1

    let private RenderMessage
            (spriteBatch : SpriteBatch)
            (row : int ref)
            (hue : Hue, text : string)
            : unit =
        let column = ref 0
        text.ToCharArray()
        |> Array.toList
        |> List.iter
            (fun character ->
                RenderCharacter spriteBatch (column, row.Value) (hue, character))
        row := row.Value + 1

    let private RenderMessages
            (context : CommonContext)
            (spriteBatch : SpriteBatch)
            : unit =
        let messages = Presentation.Display.GetMessages context
        let row = ref 0
        messages
        |> List.iter
            (fun message ->
                RenderMessage spriteBatch row message)

    let internal Render
            (context : CommonContext)
            (session : SessionIdentifier)
            (spriteBatch:SpriteBatch) 
            : unit =
        spriteBatch.Begin()
        RenderLayout spriteBatch
        RenderField context session spriteBatch
        RenderMessages context spriteBatch
        spriteBatch.End()
