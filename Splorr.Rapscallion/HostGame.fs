﻿namespace Splorr.Rapscallion

open Microsoft.Xna.Framework
open Microsoft.Xna.Framework.Graphics
open Microsoft.Xna.Framework.Input
open System.IO
open System
open Splorr.Common
open Splorr.Rapscallion.Presentation
open Splorr.Rapscallion.Model

type HostGame(context : CommonContext) as this =
    inherit Game()

    do
        this.Content.RootDirectory <- "Content"

    let context = context
    let session = Business.Game.CreateSessionIdentifier context

    let graphics = new GraphicsDeviceManager(this)
    let spriteBatch : SpriteBatch ref = ref null

    let InitializeGraphics() : unit =
        graphics.PreferredBackBufferWidth <- Constants.BackBufferWidth
        graphics.PreferredBackBufferHeight <- Constants.BackBufferHeight
        graphics.ApplyChanges()

    let InitializeGame() : unit =
        Presentation.Display.SetFieldSize context (Constants.FieldColumns, Constants.FieldRows)
        Presentation.Display.SetMessagesSize context (Constants.MessageColumns, Constants.MessageRows)
        Presentation.Display.PutMessage context (Cyan, "Rapscallion of SPLORR!!")
        Business.Game.PutLocationForSession context session (Some (Constants.FieldColumns/2, Constants.FieldRows/2))

    member private this.InitializeWindow() =
        this.Window.Title <- "Rapscallion of SPLORR!!"
        this.Window.TextInput.Add(InputHandler.OnTextInput)
        this.Window.KeyDown.Add(InputHandler.OnKeyDown context session)
        this.Window.KeyUp.Add(InputHandler.OnKeyUp)

    override this.Initialize() =
        this.InitializeWindow()
        InitializeGraphics()
        spriteBatch := new SpriteBatch(this.GraphicsDevice)
        InitializeGame()
        base.Initialize()

    override this.LoadContent() =
        Renderer.Load this.GraphicsDevice
    
    override _.Update (_:GameTime)  =
        ()

    override this.Draw (_:GameTime) =
        Color.Black
        |>  this.GraphicsDevice.Clear
        Renderer.Render context session spriteBatch.Value 
