﻿namespace Splorr.Rapscallion

open Splorr.Common
open Splorr.Rapscallion.Presentation
open Splorr.Rapscallion.Persistence
open Splorr.Rapscallion.Business
open Splorr.Rapscallion.Model
open System

type internal RapscallionContext() =
    inherit PersistenceContext()
    interface CommonContext
    interface PresentationContext
    interface Field.SetSizeContext with
        member this.fieldSizeSink = ref FieldImplementation.SetSize
    interface Field.PutCellContext with
        member this.fieldCellSink = ref FieldImplementation.PutCell
    interface Messages.SetSizeContext with
        member this.messagesSizeSink =  ref MessagesImplementation.SetSize
    interface Messages.PutContext with
        member this.messageSink = ref MessagesImplementation.Put
    interface Messages.GetContext with
        member this.messagesSource = ref MessagesImplementation.Get
    interface Field.GetCellsContext with
        member this.fieldCellSource = ref FieldImplementation.GetCells
