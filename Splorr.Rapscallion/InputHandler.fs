﻿namespace Splorr.Rapscallion

open Microsoft.Xna.Framework
open Microsoft.Xna.Framework.Input

open Splorr.Common
open Splorr.Rapscallion.Presentation
open Splorr.Rapscallion.Model
open Splorr.Rapscallion.Business

module internal InputHandler =
    let internal OnTextInput (args: TextInputEventArgs) : unit =
        ()//Keep - eventually text input will be a thing

    let private Move 
            (context : CommonContext)
            (session : SessionIdentifier)
            (message : Hue * string)
            (moveBy: Location)
            : unit =
        Game.GetLocationForSession context session
        |> Option.iter
            (fun position ->
                Presentation.Display.PutMessage context message
                ((position |> fst) + (moveBy |> fst), (position |> snd) + (moveBy |> snd))
                |> Some
                |> Game.PutLocationForSession context session)

    let internal OnKeyDown
            (context : CommonContext)
            (session : SessionIdentifier)
            (args:InputKeyEventArgs) 
            : unit =
        match args.Key with
        | Keys.Up ->
            Move context session (White, "North") (0,-1)
        | Keys.Down ->
            Move context session (White, "South") (0,1)
        | Keys.Left ->
            Move context session (White, "West") (-1,0)
        | Keys.Right ->
            Move context session (White, "East") (1,0)
        | _ ->
            ()


    let internal OnKeyUp (args:InputKeyEventArgs) : unit =
        ()//keep - eventually releasing a key MAY be a thing


