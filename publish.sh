#!/bin/bash

rm -rf ./deploy/win/*
#rm -rf ./deploy/mac/*
#rm -rf ./deploy/linux/*
#rm -rf ./deploy/pi/*
rm -rf ./deploy/*.zip

dotnet publish -c Release --runtime win-x64 -o ./deploy/win ./Splorr.Rapscallion/Splorr.Rapscallion.fsproj -p:PublishReadyToRun=true
#dotnet publish -c Release --runtime osx-x64 --framework netcoreapp3.1 --self-contained true  -o ./deploy/mac ./Splorr.Rapscallion/Splorr.Rapscallion.fsproj
#dotnet publish -c Release --runtime linux-x64 --framework netcoreapp3.1 --self-contained true  -o ./deploy/linux ./Splorr.Rapscallion/Splorr.Rapscallion.fsproj
#dotnet publish -c Release --runtime linux-arm64 --framework netcoreapp3.1 --self-contained true  -o ./deploy/pi ./Splorr.Rapscallion/Splorr.Rapscallion.fsproj

rm ./deploy/win/*.pdb
#rm ./deploy/mac/*.pdb
#rm ./deploy/linux/*.pdb
#rm ./deploy/pi/*.pdb
"C:\Program Files\7-Zip\7z" a -tzip ./deploy/Rapscallion-win.zip ./deploy/win/*
#"C:\Program Files\7-Zip\7z" a -tzip ./deploy/Rapscallion-mac.zip ./deploy/mac/*
#"C:\Program Files\7-Zip\7z" a -tzip ./deploy/Rapscallion-linux.zip ./deploy/linux/*
#"C:\Program Files\7-Zip\7z" a -tzip ./deploy/Rapscallion-pi.zip ./deploy/pi/*
