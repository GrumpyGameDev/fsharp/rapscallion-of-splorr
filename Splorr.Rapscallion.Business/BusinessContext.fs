﻿namespace Splorr.Rapscallion.Business

open Splorr.Common

type BusinessContext =
    inherit CommonContext
    inherit SessionIdentifierRepository.CreateContext
    inherit SessionLocation.GetContext
    inherit SessionLocation.PutContext
