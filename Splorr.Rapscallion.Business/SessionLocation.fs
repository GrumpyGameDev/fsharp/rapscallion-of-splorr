﻿namespace Splorr.Rapscallion.Business

open Splorr.Common
open Splorr.Rapscallion.Model
open System

module SessionLocation = 
    type SessionLocationSink = SessionIdentifier * Location option -> unit
    type PutContext =
        abstract member sessionLocationSink : SessionLocationSink ref
    let internal Put
            (context : CommonContext) =
        (context :?> PutContext).sessionLocationSink.Value

    type SessionLocationSource = SessionIdentifier -> Location option
    type GetContext = 
        abstract member sessionLocationSource : SessionLocationSource ref
    let internal Get
            (context : CommonContext) =
        (context :?> GetContext).sessionLocationSource.Value


