﻿namespace Splorr.Rapscallion.Business

open Splorr.Rapscallion.Model
open Splorr.Common

module SessionIdentifierRepository =
    type SessionIdentifierSource = unit -> SessionIdentifier
    type CreateContext =
        abstract member sessionIdentifierSource : SessionIdentifierSource ref
    let internal Create
            (context : CommonContext) =
        (context :?> CreateContext).sessionIdentifierSource.Value()
