﻿namespace Splorr.Rapscallion.Business

open Splorr.Common
open Splorr.Rapscallion.Model
open System

module Game =
    let CreateSessionIdentifier = SessionIdentifierRepository.Create
    let GetLocationForSession = SessionLocation.Get
    let PutLocationForSession 
            (context : CommonContext)
            (session : SessionIdentifier)
            (location : Location option)
            : unit = 
        SessionLocation.Put context (session, location)



