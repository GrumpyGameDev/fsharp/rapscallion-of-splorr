﻿module PutLocationForSessionTests

open NUnit.Framework
open Splorr.Rapscallion.Business
open System
open Splorr.Tests.Common

[<Test>]
let ``PutLocationForSession.It puts the location for the given session.`` () =
    let calledSetSessionLocation = ref false
    let context = Contexts.TestContext()
    (context :> SessionLocation.PutContext).sessionLocationSink :=
        Spies.Expect(calledSetSessionLocation, (Dummies.ValidSessionIdentifier, Some Dummies.ValidLocation))
    Game.PutLocationForSession context Dummies.ValidSessionIdentifier (Some Dummies.ValidLocation)
    Assert.IsTrue(calledSetSessionLocation.Value)

