module Contexts

open Splorr.Rapscallion.Business
open Splorr.Tests.Common
open System

type TestContext() =
    interface BusinessContext
    interface SessionIdentifierRepository.CreateContext with
        member val sessionIdentifierSource = ref (Fakes.Source ("SessionIdentifierRepository.CreateContext", Guid.Empty))
    interface SessionLocation.GetContext with
        member val sessionLocationSource = ref (Fakes.Source ("SessionLocation.GetContext", None))
    interface SessionLocation.PutContext with
        member val sessionLocationSink = ref (Fakes.Sink "SessionLocation.PutContext")