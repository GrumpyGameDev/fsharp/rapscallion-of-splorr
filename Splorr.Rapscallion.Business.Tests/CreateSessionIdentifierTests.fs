﻿module CreateSessionIdentifierTests

open NUnit.Framework
open Splorr.Rapscallion.Business
open System
open Splorr.Tests.Common

[<Test>]
let ``CreateSessionIdentifier.It creates a session identifier.``() =
    let calledCreateSessionIdentifier = ref false
    let context = Contexts.TestContext()
    (context :> SessionIdentifierRepository.CreateContext).sessionIdentifierSource :=
        Spies.Source(calledCreateSessionIdentifier, Guid.NewGuid())
    let actual = Game.CreateSessionIdentifier context
    Assert.AreNotEqual(Guid.Empty, actual)
    Assert.IsTrue(calledCreateSessionIdentifier.Value)