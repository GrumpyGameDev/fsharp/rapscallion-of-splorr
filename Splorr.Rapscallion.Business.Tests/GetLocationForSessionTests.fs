﻿module GetLocationForSessionTests

open NUnit.Framework
open Splorr.Rapscallion.Business
open System
open Splorr.Tests.Common

[<Test>]
let ``GetLocationForSession.It gets the location for the session.`` () =
    let calledGetSessionLocation = ref false
    let context = Contexts.TestContext()
    (context :> SessionLocation.GetContext).sessionLocationSource := 
        Spies.Source(calledGetSessionLocation, None)
    let actual = Game.GetLocationForSession context Dummies.ValidSessionIdentifier
    Assert.AreEqual(None, actual)
    Assert.IsTrue(calledGetSessionLocation.Value)

