# Rapscallion of SPLORR!!

A roguelike set in the world of SPLORR!!

# "Our" Definition of Roguelike

We know that there are some controversial interpretations of what makes something a Roguelike.
So, we take the factors mentioned in the Berlin interpretation, and we pick and choose our factor so that we have what a roguelike means to us.

## Included Factors

1. Random environment generation
2. Permadeath
3. Turn based
4. Grid-based
5. Resource management
6. Exploration and discovery
7. Hack'n'slash
8.  Single player character
9.  Monsters are similar to players
10. Tactical challenge
11. Numbers

## Eliminated Factors

1. Non-modal
2. Complexity

## Modified Factors

1.  ASCII display
    1.  likely to start as ascii
    2.  we wish to keep it simple and icon/glyph based when not ascii anymore
2.  Dungeons
    1.  not limited to just dungeons
    2.  somewhat like Krostdi Island

## Notes

1. cell size as 16 x 16 px
2. The play field is 45 x 45 cells (called - field)
3. minimap is 16 x 16 cells or 256 x 256 px (called minimap)
4. messages panel is 16 x 28 cells (called messages)
5. character panel(?) is 17 x 22 cells (called ???)
6. inventory panel (?) is 17 x 22 cells (called ???)
7. The "level" size is 256 x 256 px because minimap is this size
8. view always has the player at the center


 