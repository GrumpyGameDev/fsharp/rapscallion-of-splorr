﻿namespace Splorr.Rapscallion.Presentation

module Display = 
    let GetFieldCells = Field.GetCells
    let GetMessages = Messages.Get
    let PutFieldCell = Field.PutCell
    let PutMessage = Messages.Put
    let SetFieldSize = Field.SetSize
    let SetMessagesSize = Messages.SetSize

