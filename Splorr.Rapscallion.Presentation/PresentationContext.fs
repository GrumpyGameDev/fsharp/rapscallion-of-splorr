﻿namespace Splorr.Rapscallion.Presentation

open Splorr.Common

type PresentationContext =
    inherit CommonContext
    inherit Field.GetCellsContext
    inherit Field.PutCellContext
    inherit Field.SetSizeContext
    inherit Messages.GetContext
    inherit Messages.PutContext
    inherit Messages.SetSizeContext
