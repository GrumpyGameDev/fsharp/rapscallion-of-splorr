﻿namespace Splorr.Rapscallion.Presentation

open Splorr.Common
open System
open Splorr.Rapscallion.Model

module Field =
    type FieldSizeSink = Location -> unit
    type SetSizeContext =
        abstract member fieldSizeSink : FieldSizeSink ref
    let internal SetSize
            (context : CommonContext) =
        (context :?> SetSizeContext).fieldSizeSink.Value

    type FieldCellSink = Location * DisplayCell option -> unit
    type PutCellContext =
        abstract member fieldCellSink : FieldCellSink ref
    let internal PutCell
            (context : CommonContext) =
        (context :?> PutCellContext).fieldCellSink.Value

    type FieldCellSource = unit -> (Location * DisplayCell) list
    type GetCellsContext =
        abstract member fieldCellSource : FieldCellSource ref
    let internal GetCells
            (context : CommonContext) =
        (context :?> GetCellsContext).fieldCellSource.Value()



