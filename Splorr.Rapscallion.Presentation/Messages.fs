﻿namespace Splorr.Rapscallion.Presentation

open Splorr.Common
open System

module Messages =
    type MessagesSizeSink = int * int -> unit
    type SetSizeContext =
        abstract member messagesSizeSink : MessagesSizeSink ref
    let internal SetSize
            (context : CommonContext) =
        (context :?> SetSizeContext).messagesSizeSink.Value

    type MessageSink = Hue * string -> unit
    type PutContext =
        abstract member messageSink : MessageSink ref
    let internal Put
            (context : CommonContext) =
        (context :?> PutContext).messageSink.Value

    type MessagesSource = unit -> (Hue * string) list
    type GetContext = 
        abstract member messagesSource : MessagesSource ref
    let internal Get
            (context : CommonContext) =
        (context :?> GetContext).messagesSource.Value()