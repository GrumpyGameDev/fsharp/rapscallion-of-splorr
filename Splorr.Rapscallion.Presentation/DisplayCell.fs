﻿namespace Splorr.Rapscallion.Presentation

type DisplayCell =
    {
        backgroundHue : Hue
        foregroundHue : Hue
        glyph : byte
    }

