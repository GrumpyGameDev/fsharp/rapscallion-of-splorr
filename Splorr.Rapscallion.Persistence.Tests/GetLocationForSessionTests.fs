﻿module GetLocationForSession

open NUnit.Framework
open Splorr.Rapscallion.Persistence
open Splorr.Rapscallion.Business
open System

[<Test>]
let ``PutLocationForSessios.It puts a location for a session.`` () =
    let context = PersistenceContext()
    Game.PutLocationForSession context Dummies.ValidSessionIdentifier (Some Dummies.ValidLocation)
    let actual = Game.GetLocationForSession context Dummies.ValidSessionIdentifier
    Assert.AreEqual(Some Dummies.ValidLocation, actual)
    Game.PutLocationForSession context Dummies.ValidSessionIdentifier None
    let actual = Game.GetLocationForSession context Dummies.ValidSessionIdentifier
    Assert.AreEqual(None, actual)




