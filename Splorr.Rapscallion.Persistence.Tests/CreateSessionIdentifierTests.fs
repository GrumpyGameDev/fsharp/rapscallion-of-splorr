﻿module CreateSessionIdentifierTests

open NUnit.Framework
open Splorr.Rapscallion.Persistence
open Splorr.Rapscallion.Business
open System

[<Test>]
let ``Create.It generates a new session identifier.`` () =
    let context = PersistenceContext()
    let actual = Game.CreateSessionIdentifier context
    Assert.AreNotEqual(Guid.Empty, actual)
