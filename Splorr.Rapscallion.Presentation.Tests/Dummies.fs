﻿module Dummies

open Splorr.Rapscallion.Presentation

let ValidFieldSize = (45, 45)
let ValidDisplayCell : DisplayCell=
    {
        foregroundHue = Black
        backgroundHue = Black
        glyph = 0uy
    }
let ValidMessageSize = (32, 56)