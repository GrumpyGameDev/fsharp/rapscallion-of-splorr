﻿module Messages.GetTests

open NUnit.Framework
open Splorr.Rapscallion.Presentation
open Splorr.Tests.Common

[<Test>]
let ``Get.It gets all messages.`` () =
    let calledGetMessages = ref false
    let context = Contexts.TestContext()
    (context :> Messages.GetContext).messagesSource := Spies.Source(calledGetMessages, [])
    let actual =
        Display.GetMessages context
    Assert.AreEqual([], actual)
    Assert.IsTrue(calledGetMessages.Value)
    
