﻿module Messages.PutTests

open NUnit.Framework
open Splorr.Rapscallion.Presentation
open Splorr.Tests.Common

[<Test>]
let ``Put.It puts a message.`` () =
    let calledPutMessage = ref false
    let context = Contexts.TestContext()
    (context :> Messages.PutContext).messageSink := Spies.Sink(calledPutMessage)
    Display.PutMessage context (White, "This is a message")
    Assert.IsTrue(calledPutMessage.Value)
    
