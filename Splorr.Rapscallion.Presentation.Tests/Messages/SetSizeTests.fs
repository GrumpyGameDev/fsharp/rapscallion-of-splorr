﻿module Messages.SetSizeTests

open NUnit.Framework
open Splorr.Rapscallion.Presentation
open Splorr.Tests.Common

[<Test>]
let ``SetMessagesSize.It sets the size of the display.`` () =
    let calledSetSize = ref false
    let context = Contexts.TestContext()
    (context :> Messages.SetSizeContext).messagesSizeSink := Spies.Sink(calledSetSize)
    Display.SetMessagesSize context Dummies.ValidFieldSize
    Assert.IsTrue(calledSetSize.Value)

