module Contexts

open Splorr.Common
open Splorr.Rapscallion.Presentation
open Splorr.Tests.Common

type TestContext() =
    interface CommonContext
    interface PresentationContext
    interface Field.SetSizeContext with
        member val fieldSizeSink = ref (Fakes.Sink "Field.SetSizeContext")
    interface Field.PutCellContext with
        member val fieldCellSink = ref (Fakes.Sink "Field.PutCellContext")
    interface Messages.SetSizeContext with
        member val messagesSizeSink = ref (Fakes.Sink "Messages.SetSizeContext")
    interface Messages.PutContext with
        member val messageSink = ref (Fakes.Sink "Messages.PutContext")
    interface Messages.GetContext with
        member val messagesSource = ref (Fakes.Source ("Messages.GetContext", []))
    interface Field.GetCellsContext with
        member val fieldCellSource = ref (Fakes.Source("Field.GetCellsContext", []))


