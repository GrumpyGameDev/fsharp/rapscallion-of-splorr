﻿module Display.SetOutputSizeTests

open NUnit.Framework
open Splorr.Rapscallion.Presentation
open Splorr.Tests.Common

[<Test>]
let ``SetFieldSize.It sets the size of the display.`` () =
    let calledSetSize = ref false
    let context = Contexts.TestContext()
    (context :> Field.SetSizeContext).fieldSizeSink := Spies.Sink(calledSetSize)
    Display.SetFieldSize context Dummies.ValidFieldSize
    Assert.IsTrue(calledSetSize.Value)

