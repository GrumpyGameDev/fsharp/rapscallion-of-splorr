﻿module Display.GetCellsTests

open NUnit.Framework
open Splorr.Rapscallion.Presentation
open Splorr.Tests.Common


[<Test>]
let ``PutFieldCell.It puts a cell onto the field.`` () =
    let calledPutCell = ref false
    let context = Contexts.TestContext()
    (context :> Field.PutCellContext).fieldCellSink := Spies.Sink(calledPutCell)
    Display.PutFieldCell context ((0, 0), Some Dummies.ValidDisplayCell)
    Assert.IsTrue(calledPutCell.Value)

