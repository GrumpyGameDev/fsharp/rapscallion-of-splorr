﻿module Display.PutCellTests

open NUnit.Framework
open Splorr.Rapscallion.Presentation
open Splorr.Tests.Common

[<Test>]
let ``GetFieldCells.It gets the cells from the field.`` () =
    let calledGetCells = ref false
    let context = Contexts.TestContext()
    (context :> Field.GetCellsContext).fieldCellSource := Spies.Source(calledGetCells, [])
    let actual = Display.GetFieldCells context
    Assert.AreEqual([], actual)
    Assert.IsTrue(calledGetCells.Value)

